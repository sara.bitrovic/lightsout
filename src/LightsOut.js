import React, { useState, useEffect } from "react";
import "./LightsOut.css";

function LightsOut() {
  const [board, setBoard] = useState([]);

  useEffect(() => {
    generateBoard();
  }, []);

  function generateBoard() {
    const newBoard = Array(5)
      .fill()
      .map(() => Array(5).fill(true));

    for (let i = 0; i < 10; i++) {
      const row = Math.floor(Math.random() * 5);
      const col = Math.floor(Math.random() * 5);
      handleClick(row, col, newBoard);
    }
    

    const solve = isSolvable(newBoard);
    setBoard(newBoard);
    if (!solve) {
      const result = window.confirm('This pattern is unsolvable. Do you want to continue?');
      if (!result) {
        generateBoard();
      } 
    } 
  }

  // function isSolvable(pattern) {
  //   const n = pattern.length;
  //   let lights = 0;
  //   for (let i = 0; i < n; i++) {
  //     for (let j = 0; j < n; j++) {
  //       if (pattern[i][j] === 1) {
  //         lights++;
  //       }
  //     }
  //   }
  //   return lights % 2 === 0;
  // }

  function matrixModMultiply(matrix, vector) {
    const mod = 2; 
    const m = matrix.length;
    const n = vector[0].length;
  
    const result = Array.from({ length: m }, () => Array.from({ length: n }, () => 0)); 
// Array.from({ length: m }, () => ...) kreira niz dužine m i primjenjuje funkciju za svaki element. Generise redove matrice
// Array.from({ length: n }, () => 0) kreira niz dužine n i svakom elementu dodjeljuje vrijednost 0. Predstavlja red u matrici
// Spoljni Array.from generiše m redova izvršavanjem unutrašnjeg Array.from za svaki red, 
// -> što rezultira matricom sa m redova i n kolona, gdje je svaki element inicijalizovan na 0.
  
    for (let i = 0; i < m; i++) {
      for (let j = 0; j < n; j++) {
        let sum = 0;
        for (let k = 0; k < m; k++) {
          sum += matrix[i][k] * vector[k][j];
          // On izračunava skalarni proizvod između reda matrice i kolone vektora.
          // U kontekstu množenja matrice, kada se red matrice množi kolonom vektora, rezultujuća vrijednost je skalar.
          // Dobija se uzimanjem tačkastog proizvoda reda i kolone, što podrazumijeva množenje odgovarajućih elemenata i njihovo sabiranje.
        }
        result[i][j] = sum % mod;
      }
    }
    return result;
  }
  
  function matrixInverse(matrix) {
    const n = matrix.length;
    const identity = Array.from({ length: n }, (_, i) =>
      Array.from({ length: n }, (_, j) => (i === j ? 1 : 0))
    );
  
    // Izvođenje Gaussove eliminacije osigurava da su najveće vrijednosti odabrane kao pivoti, 
    // smanjujući mogućnost numeričke nestabilnosti i poboljšavajući točnost rezultata. 
    // To je uobičajena tehnika koja se koristi u rješavanju sistema linearnih jednacina i pronalaženju inverznih matrica.
    for (let i = 0; i < n; i++) {
      let maxRow = i;
      for (let j = i + 1; j < n; j++) {
        if (Math.abs(matrix[j][i]) > Math.abs(matrix[maxRow][i])) {
          maxRow = j;
        }
      }
  
      [matrix[i], matrix[maxRow]] = [matrix[maxRow], matrix[i]];
      [identity[i], identity[maxRow]] = [identity[maxRow], identity[i]];
  
      if (matrix[i][i] === 0) {
        throw new Error('Matrix is singular');
      }
  
      const scalar = 1 / matrix[i][i];
      for (let j = 0; j < n; j++) {
        matrix[i][j] *= scalar;
        identity[i][j] *= scalar;
      }
  
      for (let j = 0; j < n; j++) {
        if (j !== i) {
          const factor = matrix[j][i];
          for (let k = 0; k < n; k++) {
            matrix[j][k] -= factor * matrix[i][k];
            identity[j][k] -= factor * identity[i][k];
          }
        }
      }
    }
  
    return identity;
  }
  
  function isSolvable(pattern) {
    const m = pattern.length;
    const n = pattern[0].length;
  
    const M = Array.from({ length: m * n }, () => Array.from({ length: m * n }, () => 0));
  
    for (let i = 0; i < m; i++) {
      for (let j = 0; j < n; j++) {
        const index = i * n + j;
        M[index][index] = 1;  // Diagonal elements
        if (i > 0) {
          M[index][(i - 1) * n + j] = 1;  // Top neighbor
        }
        if (i < m - 1) {
          M[index][(i + 1) * n + j] = 1;  // Bottom neighbor
        }
        if (j > 0) {
          M[index][i * n + (j - 1)] = 1;  // Left neighbor
        }
        if (j <      n - 1) {
          M[index][i * n + (j + 1)] = 1;  // Right neighbor
        }
      }
    }
  
    // Convert the pattern into a column vector
    const b = pattern.flat().map((value) => [value]);
  
    // Compute the solution using matrix inverse
    try {
      const M_inv = matrixInverse(M);  // Compute the inverse of M
      const t = matrixModMultiply(M_inv, b);  // Perform matrix multiplication (mod 2)
  
      // Check if the solution exists
      for (let i = 0; i < t.length; i++) {
        for (let j = 0; j < t[0].length; j++) {
          if (t[i][j] !== 0) {
            return true; // Solution exists, pattern is solvable
          }
        }
      }
      return false; // No solution exists, pattern is unsolvable
    } catch (error) {
      return false; // If inverse doesn't exist, the pattern is unsolvable
    }
  }
  

  function handleClick(row, col, currBoard) {
    const newBoard = [...currBoard];
    newBoard[row][col] = !newBoard[row][col];

    if (row > 0) {
      newBoard[row - 1][col] = !newBoard[row - 1][col];
    }
    if (row < newBoard.length - 1) {
      newBoard[row + 1][col] = !newBoard[row + 1][col];
    }
    if (col > 0) {
      newBoard[row][col - 1] = !newBoard[row][col - 1];
    }
    if (col < newBoard[row].length - 1) {
      newBoard[row][col + 1] = !newBoard[row][col + 1];
    }
    const isWin = currBoard.every(row => row.every(val => val === false));
    if (isWin) {
      alert("Congratulations, you won!");
      generateBoard(); 
    }
    else {
        return setBoard(newBoard);
    }
  }

  // function determinant(matrix) {
  //   let det = 0;
  //   for (let i = 0; i < matrix.length; i++) {
  //     let j = i + 1;
  //     if (j >= matrix.length) {
  //       j = 0;
  //     }
  //     let k = j + 1;
  //     if (k >= matrix.length) {
  //       k = 0;
  //     }
  //     let l = k + 1;
  //     if (l >= matrix.length) {
  //       l = 0;
  //     }

  //     det +=
  //       matrix[i][0] *
  //       matrix[j][1] *
  //       matrix[k][2] *
  //       matrix[l][3] *
  //       matrix[i][4] -
  //       matrix[i][4] *
  //       matrix[j][3] *
  //       matrix[k][2] *
  //       matrix[l][1] *
  //       matrix[i][0];
  //   }

  //   return det;
  // }

  function handleRestart() {
    generateBoard();
  }

  return (
    <div className="board">
      {board.map((row, rowIndex) => (
        <div key={rowIndex}>
          {row.map((cell, colIndex) => (
            <button
              key={colIndex}
              className={cell ? "on" : "off"}
              onClick={() => handleClick(rowIndex, colIndex, board)}
            />
          ))}
        </div>
      ))}
      <button className="restart" onClick={handleRestart}>
        RESTART
      </button>
    </div>
  );
}

export default LightsOut;
