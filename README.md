LightsOut Igrica

Ovo je LightsOut igra, jednostavna logička igra u kojoj cilj igrača je da isključi sva svijetla na tabli. Igra se sastoji od kvadratne table koja sadrži svijetla, a svako svijetlo može biti uključeno ili isključeno. Kada igrač klikne na svijetlo, ono mijenja svoje stanje, kao i stanje svih susjednih polja. Cilj je da se isključe sva svijetla i postigne pobjeda.

Uputstva za instalaciju

1. Klonirati projekat na svom racunaru.
2. Pozicionirati terminal u root direktorijum (lightsout).
3. npm start 

Cilj je isključiti sva svijetla na tabli.
Pobneda se postiže kada su sva svijetla isključena.
Dodatne informacije

Ova igra je razvijena u ReactJS-u.
Implementirana je jednostavna logika koja mijenja stanje polja na osnovu korisnikovog klika.
Korisnički interfejs je dizajniran da bude jednostavan i intuitivan za korišćenje.

Takodje, implementirana je funkcija isSolvable koja provjerava da li pocetni sablon ima rjesenje, ako postoji inverzna matrica pocetne matrice - pocetni sablon ima rjesenje. 

Created by Sara Bitrovic.